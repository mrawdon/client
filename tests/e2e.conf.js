exports.config = {
    seleniumAddress: 'http://localhost:32771/wd/hub',
    baseUrl: 'http://172.17.0.10/',
    suites: {
        signin: './e2e/signin/*_spec.js',
        board: './e2e/board/*_spec.js'
    },
    multiCapabilities: [
        {
            browserName: 'chrome'
        },
        {
            browserName: 'firefox'
        }
    ]
};
