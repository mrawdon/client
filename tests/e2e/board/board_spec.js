var mockModule = require('./../../application.mock.js');

var mocks = require('./../../mocks/mocks.js').mocks;

describe('Board module', function () {
    beforeEach(function () {
        browser.addMockModule('ngMockE2E', function () {
            var js = document.createElement("script");

            js.type = "text/javascript";
            js.src = '/assets/js/angular-mocks.js';

            document.body.appendChild(js);
        });

        browser.addMockModule('gitlabKBAppMock', mockModule.httpBackendMock, mocks);
    });

    it('Should show boards list to authenticated user', function () {
        browser.get('/boards/');
        var projects = element.all(by.repeater('project in group'));
        expect(projects.count()).toEqual(mocks.boards.data.length * 2);
    });

    it('Should show cards on board', function () {
        browser.get('/boards/kanban/client');
        browser.waitForAngular();
        expect(element.all(by.css('.card')).count()).toEqual(mocks.cards.data.length);
    });

    it('should open modal when click on card', function () {
        var link = element(by.linkText('Issue #171'));
        link.click();

        browser.waitForAngular();

        expect(browser.getLocationAbsUrl()).toEqual('/boards/kanban/client/issues/171');
        expect(browser.isElementPresent(element(by.css('.body-modal')))).toBe(true);
    });
});



