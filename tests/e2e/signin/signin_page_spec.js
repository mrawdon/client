var SigninPage = function () {
    this.login = element(by.model('data.signin.username'));
    this.password = element(by.model('data.signin.password'));
    this.button = element(by.id('signin-button'));

    this.get = function () {
        browser.get('/');
        return this;
    };

    this.setLogin = function (login) {
        this.login.sendKeys(login);
        return this;
    };

    this.setPassword = function (password) {
        this.password.sendKeys(password);
        return this;
    };
};

module.exports = SigninPage;
