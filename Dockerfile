FROM leanlabs/nginx:1.0.1

ADD . /var/www/client/
ADD ./build/sites-enabled/client.conf /etc/nginx/sites-enabled/client.conf

ENTRYPOINT ["/bin/sh", "/var/www/client/build/entry.sh"]

CMD ["nginx", "-g", "daemon off;"]
